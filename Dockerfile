# Build stage:
FROM node:18.16-alpine as build
#V0.1.7

ENV NODE_ENV production

COPY . /src
WORKDIR /src
    
RUN yarn global add sodium-native 
RUN yarn install --prod

# Prod stage
FROM node:18.16-alpine

COPY --from=build /src /srv
WORKDIR /srv

CMD ["node", "app.js"]
